<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karafuru extends Model
{
    protected $table = 'karafurus';

    protected $guarded = ['id'];
}
