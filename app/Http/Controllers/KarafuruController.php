<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Karafuru;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;

class KarafuruController extends Controller
{
/**
* The layout that should be used for responses.
*/
// protected $layout = 'layouts.master';

/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
    $karafuru = Karafuru::orderBy('created_at', 'desc')->get();
    return View('index',compact('karafuru'));
}

public function popular()
{
    $karafuru = Karafuru::orderBy('upvotes', 'desc')->get();
    return View('popular',compact('karafuru'));
}

/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
    return View('create');
}

public function about()
{
    return View('about');
}

/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store()
{
    if(\Request::ajax()){
        try{
            $input = \Input::all();
            $karafuru = new Karafuru;
            $karafuru->color1 = $input['color1'];
            $karafuru->color2 = $input['color2'];
            $karafuru->color3 = $input['color3'];
            $karafuru->color4 = $input['color4'];
            $karafuru->save();
        }catch(\ModelNotExpection $e){
            return response()->json(['code' => 404]);
        }catch(\Exception $ex){
            return response()->json(['code' => 405]);
        }
    }

    return response()->json(['code' => 200,'msg' => 'success']);
}

/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function k($id)
{
    $karafuru = Karafuru::find($id);
    return View('k',compact('karafuru'));
}

/*
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request)
{
    $input = $request->all();
    $id = $input['id'];

    $sessions = DB::table('session_vote')
    ->where('session',Session::getId())
    ->where('karafuru_id',$id)->first();
    
    if (!$sessions) {
        $karafuru = Karafuru::find($id);
        $karafuru->upvotes += 1;
        $karafuru->save();

        DB::table('session_vote')
        ->insert([
            'session'=> Session::getId(),
            'karafuru_id' => $id
            ]);

        return response()->json(['code' => 200,'msg' => 'success', 'upvotes' => $karafuru->upvotes]);
    }
    return response()->json(['code' => 404,'msg' => 'fail']);
}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
    $karafuru = Karafuru::find($id);
    $karafuru->delete();

    return redirect('/');
}
}