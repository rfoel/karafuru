<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'KarafuruController@index');

Route::get('/popular', 'KarafuruController@popular');

Route::get('/create', 'KarafuruController@create');

Route::get('/popular', 'KarafuruController@popular');

Route::get('/about', 'KarafuruController@about');

Route::get('/k/{id}', 'KarafuruController@k');

Route::get('/destroy/{id}', 'KarafuruController@destroy');

Route::post('/update', 'KarafuruController@update');

Route::post('/store', 'KarafuruController@store');