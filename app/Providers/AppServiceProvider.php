<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([base_path('vendor/twbs/bootstrap/dist') => public_path('bootstrap')],'public');
        $this->publishes([base_path('vendor/components/jquery') => public_path('js')],'public');
        $this->publishes([base_path('vendor/components/font-awesome') => public_path('font-awesome')],'public');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
