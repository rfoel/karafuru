$('#done').click(function(e) {
	e.preventDefault();
	$.ajax({
		type: "POST",
		url: "store",
		data: {color1:$('.color1').val().slice(1), 
			   color2:$('.color2').val().slice(1), 
			   color3:$('.color3').val().slice(1), 
			   color4:$('.color4').val().slice(1)},
		success: function(msg) {
			console.log(msg);
			location.href='/';
		},
		error: function(msg){
			console.log(msg);
		}
	});
});

$(document).ready(function() {
	$('.jscolor').colorPicker();
});