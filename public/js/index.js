$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

$('.hex').hover(
    function(e) {
        var x = $(this).css('backgroundColor');
        var color = hexc(x);
        $('.code', this).text(color);   
    },
    function(e) {
        $('.code', this).text('');   
    }
);

function hexc(colorval) {
    var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    delete(parts[0]);
    for (var i = 1; i <= 3; ++i) {
        parts[i] = parseInt(parts[i]).toString(16);
        if (parts[i].length == 1) parts[i] = '0' + parts[i];
    }
    return '#' + parts.join('');
}

$('.vote').click(function(e) {
    e.preventDefault();
    var idk = $(this).data("id");
    $.ajax({
        type: "POST",
        url: "/update",
        data: {id:$(this).data("id")},
        success: function(msg) {
            console.log(msg);
            $('#'+idk).text(msg['upvotes']);
        },
        error: function(msg){
            console.log(msg);
        }
    });
});