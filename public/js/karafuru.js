$(function () {
   $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
   });
});

$('#done').click(function(e) {
	e.preventDefault();
	$.ajax({
		type: "POST",
		url: "store",
		data: {color1:$('.color1').val(), color2:$('.color2').val(), color3:$('.color3').val(), color4:$('.color4').val()},
		success: function(msg) {
			console.log(msg);
		},
		error: function(msg){
			console.log(msg);
		}
	});
});

$('.hex').hover(function(e) {
	var x = $(this).css('backgroundColor');
	var color = hexc(x);
	$(this).text(color);
});

$('.hex').mouseout(function(e) {
	$(this).text('');
});

function hexc(colorval) {
    var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    delete(parts[0]);
    for (var i = 1; i <= 3; ++i) {
        parts[i] = parseInt(parts[i]).toString(16);
        if (parts[i].length == 1) parts[i] = '0' + parts[i];
    }
    return '#' + parts.join('');
}