@extends('layouts.master')

@section('title', "$karafuru->id")

@section('navbar')
@parent
@endsection

@section('content')
<div class="container">
	<center>
		<div class="show">
			<div class="box">
				<div class="color1 hex text-muted" style="background: #{{$karafuru->color1}}"><p class="code"></p></div>
				<div class="color2 hex text-muted" style="background: #{{$karafuru->color2}}"><p class="code"></p></div>
				<div class="color3 hex text-muted" style="background: #{{$karafuru->color3}}"><p class="code"></p></div>
				<div class="color4 hex text-muted" style="background: #{{$karafuru->color4}}"><p class="code"></p></div>
				<div class="caption">
				<div class="upvotes text-muted"><a href="#" data-id="{{$karafuru->id}}" class="vote"><i class="fa fa-heart" aria-hidden="true"></i></a> <p id="{{$karafuru->id}}" class="count">{{$karafuru->upvotes}}</p></div>
					<div class="date text-muted">{{$karafuru->created_at->diffForHumans()}}</div>
				</div>
			</div>
		</div>
	</center>
</div>
@endsection