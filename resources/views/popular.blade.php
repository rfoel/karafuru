@extends('layouts.master')

@section('title', 'popular')

@section('navbar')
@parent
@endsection

@section('content')
@if(count($karafuru))
@foreach($karafuru as $color)
<div class="col-xs-12 col-sm-6 col-md-4">
	<div class="box">
		<a href="k/{{$color->id}}">
			<div class="color1 hex text-muted" style="background: #{{$color->color1}}"><p class="code"></p></div>
			<div class="color2 hex text-muted" style="background: #{{$color->color2}}"><p class="code"></p></div>
			<div class="color3 hex text-muted" style="background: #{{$color->color3}}"><p class="code"></p></div>
			<div class="color4 hex text-muted" style="background: #{{$color->color4}}"><p class="code"></p></div>
			<div class="caption">
				<div class="upvotes text-muted"><a href="#" data-id="{{$color->id}}" class="vote"><i class="fa fa-heart" aria-hidden="true"></i></a> <p id="{{$color->id}}" class="count">{{$color->upvotes}}</p></div>
				<div class="date text-muted">{{$color->created_at->diffForHumans()}}</div>
			</div>
		</a>
	</div>
</div>
@endforeach
@else
<center>
	<h1 class="about-heading text-muted">No karafurus to show :(</h1>
</center>
@endif
@endsection