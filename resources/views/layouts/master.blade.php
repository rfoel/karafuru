<html>
<head>
    <title>Karafuru - @yield('title')</title>
    <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/hover-min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <link href="{{ asset('favicon.ico') }}" rel="shortcut icon">
    
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>
    @section('navbar')
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <ul class="nav nav-justified">
                    <li><a href="/" class="navbar-brand">カラフル</a></li>
                </ul>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li id="center-xs" {!! (Request::is('/') ? 'class="active"' : '') !!}><a href="/" class="hvr-sweep-to-right tile1">New</a></li>
                    <li id="center-xs" {!! (Request::is('popular') ? 'class="active"' : '') !!}><a href="/popular" class="hvr-sweep-to-right tile2">Popular</a></li>
                    <li id="center-xs" {!! (Request::is('create') ? 'class="active"' : '') !!}><a href="/create" class="hvr-sweep-to-right tile3">Create</a></li>
                    <li id="center-xs" {!! (Request::is('about') ? 'class="active"' : '') !!}><a href="/about" class="hvr-sweep-to-right tile4">About</a></li>
                    <li class="visible-xs"><a href="#" class="hvr-sweep-to-right instagram"><i class="fa fa-instagram fa-lg"></i></a></li>
                    <li class="visible-xs"><a href="#" class="hvr-sweep-to-right twitter"><i class="fa fa-twitter fa-lg"></i></a></li>
                    <li class="visible-xs"><a href="#" class="hvr-sweep-to-right facebook"><i class="fa fa-facebook fa-lg"></i></a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right icon-list hidden-xs">
                    <li><a href="#" class="hvr-sweep-to-right instagram"><i class="fa fa-instagram fa-lg"></i></a></li>
                    <li><a href="#" class="hvr-sweep-to-right twitter"><i class="fa fa-twitter fa-lg"></i></a></li>
                    <li><a href="#" class="hvr-sweep-to-right facebook"><i class="fa fa-facebook fa-lg"></i></a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
    @show

    <div class="container">
        @yield('content')
    </div>

    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script> 
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/index.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/create.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/colors.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jqColorPicker.js') }}" type="text/javascript"></script>
</body>
</html>