@extends('layouts.master')
@section('title', 'create')

@section('navbar')
@parent
@endsection

@section('content')
	<div class="container">
		<center>
			<h1 class="about-heading text-muted">Create your Karafuru!</h1>
			<button id="done" class="btn btn-lg btn-default text-muted" type="submit">Done</button>
			<div class=create>
				<input name="color1" class="jscolor color1" value="#DDDDDD" maxlength="7"></input><br>
				<input name="color2" class="jscolor color2" value="#CCCCCC" maxlength="7"></input><br>
				<input name="color3" class="jscolor color3" value="#BBBBBB" maxlength="7"></input><br>
				<input name="color4" class="jscolor color4" value="#AAAAAA" maxlength="7"></input>
			</div>
		</center>
	</div>
@endsection
