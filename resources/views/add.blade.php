@extends('layouts.master')
@section('title', 'Page Title')


@section('navbar')
@parent
@endsection

@section('content')

<div class="container">
<center>
	<h1>Create a Karafuru!</h1>
	<div class=create>
		<input name="color1" class="jscolor color1" value="#ddd"></input><br>
		<input name="color2" class="jscolor color2" value="#ccc"></input><br>
		<input name="color3" class="jscolor color3" value="#bbb"></input><br>
		<input name="color4" class="jscolor color4" value="#aaa"></input>
	</div>
	<button id="done" class="btn btn-lg btn-default" type="submit">Done</button>
</center>
</div>

<script src="{{ asset('js/jscolor.js') }}" type="text/javascript"></script>

@endsection