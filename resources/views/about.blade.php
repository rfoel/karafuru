@extends('layouts.master')

@section('title', 'about')

@section('navbar')
@parent
@endsection

@section('content')
<div class="container">
	<center>
		<h1 class="about-heading text-muted">Find your Karafuru!</h1>
		<div class="box-about"></div>
		<p class="about-text text-muted">Karafuru is a plataform to create and share amazing color palettes. カラフル, or Karafuru, means colorful in Japanese, and that's what we are meant to be! Let your criativity guide you through the path of colors and create the most incredible Karafuru! Enjoy :)</p>

		<p class="made text-muted">Made by <strong>Rafael Franco</strong><br>― a man needs to code.</p>

		<a href="https://twitter.com//rfoel" target="_blank"><i class="fa fa-twitter fa-twitter2 fa-4x text-muted"></i></a>
		<a href="https://github.com/rfoel" target="_blank"><i class="fa fa-github fa-4x text-muted"></i></a>
		<a href="https://gitlab.com/u/rfoel" target="_blank"><i class="fa fa-gitlab fa-4x text-muted"></i></a>
	</center>
</div>
@endsection