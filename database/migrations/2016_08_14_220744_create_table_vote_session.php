<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVoteSession extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_vote', function (Blueprint $table) {
            $table->string('session');
            $table->integer('karafuru_id')->unsigned();
            $table->foreign('karafuru_id')->references('id')->on('karafurus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('session_vote');
    }
}
