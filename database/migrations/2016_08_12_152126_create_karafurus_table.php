<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKarafurusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karafurus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('color1', 255);
            $table->string('color2', 255);
            $table->string('color3', 255);
            $table->string('color4', 255);
            $table->integer('upvotes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('karafurus');
    }
}
